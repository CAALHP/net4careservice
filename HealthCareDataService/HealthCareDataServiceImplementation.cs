﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using PHEI.DTO;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using caalhp.Core.Contracts;
using caalhp.Core.Utils.Helpers.Serialization;
using HealthCareDataService.VitalSignService;
using System.ServiceModel;

namespace HealthCareDataService
{
    public class HealthCareDataServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private PatientServiceClient service;
        private string _cpr;
        private string _passkey;
        private BloodPressure _bpMeasurementPending;

        //private readonly INet4CareManager _oximeterManager;
        //private readonly INet4CareManager _bloodSugarManager;
        //private readonly INet4CareManager _weightManager;
        //private readonly INet4CareManager _heartRateManager;
        //private readonly BloodPressureManager _bloodPressureManager;

        public HealthCareDataServiceImplementation()
        {
            //Debugger.Launch();

            //Utils.Cpr = ConfigurationManager.AppSettings.Get("_cpr");
            //_cpr = ConfigurationManager.AppSettings.Get("_cpr");
            //_passkey = ConfigurationManager.AppSettings.Get("_passkey");
            ConnectToService();

            HandlePatientData();

            
        }

        private void ConnectToService()
        {
            try
            {
                var address = ConfigurationManager.AppSettings.Get("OpenHealthPHRserveraddress");


                BasicHttpBinding httpBinding = new BasicHttpBinding();
                httpBinding.MaxReceivedMessageSize = 2147483647;
                httpBinding.MaxBufferSize = 2147483647;


                EndpointAddress endpoint = new EndpointAddress("http://localhost:8083/VitalSignsService");
                //System.Diagnostics.Debugger.Launch();
                //TODO: refactor to dynamic adress

                service = new PatientServiceClient(httpBinding, endpoint);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while connecting to OpenHealthPHR sevic");
            }
        }

        private void HandlePatientData()
        {
            if (service == null) return;

            try
            {
                Patient[] patients = service.RetrieveAllUnassignedPatients();
                //Patient[] patients = imp.RetrieveAllPatients();

                if (patients == null || patients.Length == 0)
                {
                    CurrentPatient = null;
                    var dto = new Patient();
                    dto.HealthCareUserID = 0;
                    dto.SocialSecurity = "Test";
                    dto.PassKey = "Test";

                    CurrentPatient = service.CreateNewPatient(dto);

                }
                else
                {
                    CurrentPatient = patients[0];

                }
                _cpr = CurrentPatient.SocialSecurity;
                _passkey = CurrentPatient.PassKey;
                var result = service.RetrieveAllVitalSigns(_cpr, _passkey);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }
        }


        /*private void HandleEvent(Event theEvent)
        {
            if(theEvent.GetType() == typeof(BloodPressureMeasurementEvent))
                HandleEvent(theEvent as BloodPressureMeasurementEvent);
            else if(theEvent.GetType() == typeof(SimpleMeasurementEvent))
                HandleEvent(theEvent as SimpleMeasurementEvent);
        }*/

        /// <summary>
        /// Handles recieving a blood pressure measurement
        /// </summary>
        /// <param name="bpMeasurement"></param>
        private void HandleEvent(BloodPressureMeasurementEvent bpMeasurement)  
        {
            try {
            var vs = new BloodPressure();
            vs.Systolic = (int)bpMeasurement.Systolic;
            vs.Diastolic = (int)bpMeasurement.Diastolic;
            //vs.HeartRate = 70; //TODO: update the event
                
            vs.TimeStamp = bpMeasurement.Timestamp;
            //var service = new PatientServiceClient();

            //using (service) 
            //    service.AddVitalSign(vs, _cpr, _passkey);
            _bpMeasurementPending = vs;
            } catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }
              
        }

        /// <summary>
        /// Handles recieving all other events
        /// </summary>
        /// <param name="simpleMeasurement"></param>
        private void HandleEvent(SimpleMeasurementEvent simpleMeasurement)
        {
            try {
            Console.WriteLine("Event recieved: " + simpleMeasurement.MeasurementType);

            var service = new PatientServiceClient();

            switch (simpleMeasurement.MeasurementType)
            {
                case "Oximeter":
                    {
                        //_oximeterManager.AddMeasurement(simpleMeasurement.Value);
                        var vs = new Oximeter();
                        vs.SaturationLevel = simpleMeasurement.Value;
                        vs.TimeStamp = DateTime.Now;
                        service.AddVitalSign(vs, _cpr, _passkey);
                    } break;
                case "HeartRate":
                    {
                        var vs = new HeartRate();
                        vs.HeartRateInBPM = (int)simpleMeasurement.Value;
                        vs.TimeStamp = simpleMeasurement.Timestamp;
                        service.AddVitalSign(vs, _cpr, _passkey);
                        if (_bpMeasurementPending != null && _bpMeasurementPending.TimeStamp.Ticks == vs.TimeStamp.Ticks)
                        {
                            _bpMeasurementPending.HeartRate = vs.HeartRateInBPM;
                            service.AddVitalSign(_bpMeasurementPending, _cpr, _passkey);
                        }

                    } break;
                case "Weight":
                    {
                        //_weightManager.AddMeasurement(simpleMeasurement.Value, simpleMeasurement.UserId);
                        var vs = new Weight();
                        vs.WeigthInGrams = (int)(simpleMeasurement.Value * 1000);
                        vs.TimeStamp = simpleMeasurement.Timestamp;
                        service.AddVitalSign(vs, _cpr, _passkey);
                    } break;
                case "BloodSugar":
                    {
                        var vs = new HeartRate();
                        vs.HeartRateInBPM = (int)simpleMeasurement.Value;
                        vs.TimeStamp = simpleMeasurement.Timestamp;
                        service.AddVitalSign(vs, _cpr, _passkey);
                    } break;
            }
            } catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }    
        }

        //private void HandleEvent(UserLoggedInEvent obj)
        //{
        //    Utils.Cpr = obj.User.UserId;
        //}

        //private void HandleEvent(UserLoggedOutEvent obj)
        //{
        //    Utils.Cpr = ConfigurationManager.AppSettings.Get("cpr");
        //}

        public void Notify(KeyValuePair<string, string> notification)
        {
            try {
                //var serializer = EventHelper.GetSerializationTypeFromFullyQualifiedNameSpace(notification.Key);
                var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
                dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
                HandleEvent(obj);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }
        }

        public string GetName()
        {
            return "HealthCareDataService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {

            try
            {
                _host = hostObj;
                _processId = processId;
                _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(BloodPressureMeasurementEvent)), _processId);
                _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(SimpleMeasurementEvent)), _processId);
                _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
                _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
                //_host.Host.SubscribeToEvents(_processId);
            } catch (Exception e)
            {
                Console.WriteLine("Error during handling data: " + e);
            }
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }

        public Patient CurrentPatient { get; set; }
    }
}