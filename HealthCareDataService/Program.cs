﻿using System;
using caalhp.IcePluginAdapters;
using caalhp.Core.Contracts;
using CAALHP.SOAICE.Contracts;using Exception = Ice.Exception;

namespace HealthCareDataService
{
    public class Program
    {
        private static ServiceAdapter _adapter;
        private static IServiceCAALHPContract _implementation;

        static void Main(string[] args)
        {


            const string endpoint = "localhost";
            try
            {
                _implementation = (IServiceCAALHPContract)(new HealthCareDataServiceImplementation());
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();

        }

    }
}
