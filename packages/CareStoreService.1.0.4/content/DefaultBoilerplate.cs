﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace ServiceTemplateProject
{
    [AddIn("TemplateProject Service", Version = "1.0.0.0")]                     // Todo: Change Application name and version
    public class TemplateProjectBoilerplate : ServiceViewPluginAdapter
    {
        public TemplateProjectBoilerplate()
        {
            Service = new DefaultImplementation();                              // Todo: If the implementation class-name is changed, change it here aswel.
        }
    }
}
