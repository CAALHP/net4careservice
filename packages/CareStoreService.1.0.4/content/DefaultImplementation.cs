﻿
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using ServiceImplementationLibrary;

namespace ServiceTemplateProject                                // Todo: Change namespace to Plugins.<ApplicationName>
{                                                                   // Can be done in Project Properties -> Assembly name & Default namespace
    public class DefaultImplementation : AbstractServiceImplementation
    {

        public DefaultImplementation()                              // Todo: Implement whatever you want your application to do at app-startup
        {
            
        }

        public override string GetName()
        {
            return "MyService";                                     // Todo: Change the return strings value to your applications name
        }

        public override void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //example of subscribing to RFIDFoundEvents:
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(RFIDFoundEvent)), _processId);
        }

        public override void Start()
        {
                                                                    // Todo: Implement "Start", service handle if needed. Leave blank if not needed.
        }

        public override void Stop()
        {
                                                                    // Todo: Implement "Stop", service handle if needed. Leave blank if not needed.
        }
        
        public override bool IsAlive()
        {
            return true;
        }

        public override void ShutDown()
        {
            Environment.Exit(0);
        }
    }
}